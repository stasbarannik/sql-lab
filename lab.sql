CREATE EXTENSION pgcrypto;

SELECT gen_random_uuid();

/* Docks */
CREATE TABLE IF NOT EXISTS docks (
	id uuid PRIMARY KEY unique DEFAULT gen_random_uuid(),
	name varchar(100),
	address varchar(255)
);

INSERT INTO docks (name, address) VALUES ('Main dock', '22611 Pacific Coast Hwy Malibu'),
('Europe dock', '65012 Odessa Vice-admiral');

SELECT * from docks;

/* Workers */
CREATE TABLE IF NOT EXISTS workers (
	id uuid PRIMARY KEY unique DEFAULT gen_random_uuid(),
	first_name varchar(100),
	last_name varchar(100),
	salary float
);

INSERT INTO workers (first_name, last_name, salary) VALUES ('Ivan', 'Petrov', 1500),
('Petr', 'Ivanov', 1800),
('Sergey', 'Semenov', 1000),
('Max', 'Mad', 4300),
('Lev', 'Tolstoy', 3400),

SELECT * from workers;

/* Teams */
CREATE TABLE IF NOT EXISTS teams (
	id uuid PRIMARY KEY unique DEFAULT gen_random_uuid(),
	name varchar(100) NOT NULL
);

INSERT INTO teams (name) VALUES ('primary team'), ('secondary team');

SELECT * from teams;

/* Assignments */
CREATE TABLE IF NOT EXISTS assignments (
	id uuid PRIMARY KEY unique DEFAULT gen_random_uuid(),
	worker_id uuid NOT NULL REFERENCES workers (id),
	team_id uuid NOT NULL REFERENCES teams (id)
);

INSERT INTO assignments (worker_id, team_id) VALUES 
('03a5f522-14b3-44ba-9f89-a4d698bc0d2b', '3c53e701-b56a-4d4d-99c5-a18a0ddf88de'),
('45ba6e35-5aa4-4943-9c54-535485fdb738', '0a6bcfdf-e39a-4774-8925-b8a4926f4585'),
('c8a942b8-3a60-4e34-8fa0-6cfe87dff857', '3c53e701-b56a-4d4d-99c5-a18a0ddf88de'),
('afe6ab59-b88e-4cbb-b23c-b18661c14dd6', '0a6bcfdf-e39a-4774-8925-b8a4926f4585'),
('d604305d-52fc-4fdc-8dd0-417db4bf8260', '0a6bcfdf-e39a-4774-8925-b8a4926f4585');

SELECT * FROM assignments;

/* Job statuses */
CREATE TABLE IF NOT EXISTS job_statuses (
 status varchar(100) PRIMARY KEY unique
);

INSERT INTO job_statuses (status) VALUES ('scheduled'), ('in progress'), ('finished');

SELECT * FROM job_statuses;

/* Clients */
CREATE TABLE IF NOT EXISTS clients (
	id uuid PRIMARY KEY unique DEFAULT gen_random_uuid(),
	name varchar(100),
	phone varchar(40)
);

INSERT INTO clients (name, phone) VALUES 
('Maersk', '+1 235 456 7899'),
('FedEx', '+1 235 456 3339'),
('Kvainer', '+1 235 456 1111');

SELECT * from clients;

/* Ships */
CREATE TABLE IF NOT EXISTS ships (
	id uuid PRIMARY KEY unique DEFAULT gen_random_uuid(),
	name varchar(100),
	type varchar(100)
);

INSERT INTO ships (name, type) VALUES 
('Santa Maria', 'Galleon'),
('War king', 'Destroyer'),
('Paradise', 'Cruiser');

SELECT * from ships;

/* Services */
CREATE TABLE IF NOT EXISTS services (
	id uuid PRIMARY KEY unique DEFAULT gen_random_uuid(),
	name varchar(100) NOT NULL,
	eta interval NOT NULL,
	price float NOT NULL
);

INSERT INTO services (name, price, eta) VALUES 
('rust cleaning', 100000, '24 days'),
('undercarriage repair', 50000, '44 days'),
('hull repair', 150000, '68 days'),
('review', 3000, '5 days');

SELECT * from services;

/* Orders */
CREATE TABLE IF NOT EXISTS orders (
	id uuid PRIMARY KEY unique DEFAULT gen_random_uuid(),
	order_date TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
	expected_end_date TIMESTAMP,
	client_id uuid NOT NULL REFERENCES clients (id),
	ship_id uuid NOT NULL REFERENCES ships (id),
	dock_id uuid NOT NULL REFERENCES docks (id),
	team_id uuid NOT NULL REFERENCES teams (id),
	service_id uuid NOT NULL REFERENCES services (id),
	status varchar(100) NOT NULL REFERENCES job_statuses (status)
);

INSERT INTO orders (client_id, ship_id, dock_id, team_id, service_id, status) VALUES 
('46707b44-18b9-40b5-8e48-b5b8a5ff271e', 
 '20187542-5c55-4723-9da0-e896347dbfc8', 
 '3ec263c0-a708-4f49-bf88-1c10d7c3cc93', 
 '0a6bcfdf-e39a-4774-8925-b8a4926f4585', 
 '6f1bca6d-f156-4488-a7ee-be1277089acc',
 'in progress'),
 
('5d31800b-cc75-4366-aa5b-4652552c3fe5', 
 'c84c2cca-26fd-4cf2-a46d-637a20312204', 
 'f0c86de6-2080-4099-932f-ae4a7b2f9494', 
 '0a6bcfdf-e39a-4774-8925-b8a4926f4585',
 '6f1bca6d-f156-4488-a7ee-be1277089acc',
 'in progress'),

('5d31800b-cc75-4366-aa5b-4652552c3fe5', 
 'c84c2cca-26fd-4cf2-a46d-637a20312204', 
 'f0c86de6-2080-4099-932f-ae4a7b2f9494', 
 '3c53e701-b56a-4d4d-99c5-a18a0ddf88de', 
 '7fa1a4b6-1c3f-43f4-97f9-d69f7ea58669', 
 'scheduled'),
 
('46707b44-18b9-40b5-8e48-b5b8a5ff271e', 
 '20187542-5c55-4723-9da0-e896347dbfc8', 
 '3ec263c0-a708-4f49-bf88-1c10d7c3cc93', 
 '3c53e701-b56a-4d4d-99c5-a18a0ddf88de', 
 '85476668-61b0-46f9-9b42-99e0638223dc',
 'scheduled'),
 
('94af2e55-ac01-45d6-9197-854bd5fb29bc', 
 '749f3dfb-648d-4f46-af1e-759532c03aaf', 
 '3ec263c0-a708-4f49-bf88-1c10d7c3cc93', 
 '3c53e701-b56a-4d4d-99c5-a18a0ddf88de', 
 'f033ae11-1d7e-49a2-ab9a-96d60b1cbbcb', 
 'finished');

SELECT * from orders;


/* Procedure to estimate end work date  */
CREATE OR REPLACE FUNCTION date_calculating() RETURNS TRIGGER AS $$
	BEGIN
		NEW.expected_end_date = NEW.order_date + (SELECT eta FROM services WHERE id = NEW.service_id );
		RETURN NEW;
	END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS date_calculating ON orders;

CREATE TRIGGER date_calculating BEFORE INSERT OR UPDATE ON orders FOR EACH ROW EXECUTE PROCEDURE date_calculating();

/* Example of select data */
SELECT orders.order_date, orders.expected_end_date, orders.status,
services.name as service_name, ships.name as ship_name,
docks.name as dock_name, docks.address as dock_address, clients.name as client_name, clients.phone as client_phone
FROM orders 
LEFT JOIN services ON orders.service_id = services.id
LEFT JOIN docks ON orders.dock_id = docks.id
LEFT JOIN clients ON orders.client_id = clients.id
LEFT JOIN ships ON orders.ship_id = ships.id;
